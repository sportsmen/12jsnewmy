function circleCreate(){
	let input = document.createElement('input')
	input.id = 'input_1'
	input.placeholder = 'Введите диаметр'

	let input_2 = document.createElement('input')
	input_2.id = 'input_2'
	input_2.placeholder = 'Введите цвет'

	let button_2 = document.createElement('button')
	button_2.id = 'button_2'
	button_2.innerHTML = 'Нарисовать'


	document.getElementById('button_1').onclick = function(){
		document.getElementById('button_1').style = 'display:none;'
		document.body.appendChild(input)
		document.body.appendChild(input_2)
		document.body.appendChild(button_2)

			document.getElementById('button_2').onclick = function(){
				let diametr = document.getElementById('input_1').value
				let color = document.getElementById('input_2').value
				document.getElementById('button_2').style = 'display:none;'
				document.getElementById('input_1').style = 'display:none;'
				document.getElementById('input_2').style = 'display:none;'
				
				let div = document.createElement('div')
				div.id = 'div-circle'
				document.body.appendChild(div)
				div.style.background = color;
				div.style.width = diametr + 'px'
				div.style.height = diametr + 'px'
				div.style.borderRadius = diametr/2 + 'px'
			}		
	}
}
circleCreate()